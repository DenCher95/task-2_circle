import java.util.Scanner;

public class ScannerFunctions {
    Scanner scanner;

    public int continueOrBrake() {
        System.out.println("Желаете добавить еще (1-да 2- нет)");
        System.out.print("Ответ: ");
        scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    public double scanX() {
        System.out.println("Введите координаты точки");
        System.out.print("x: ");
        scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    public double scanY() {
        System.out.println("Введите координаты точки");
        System.out.print("y: ");
        scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    public double scanCircleRadius() {
        System.out.println("Введите радиус окружности");
        System.out.print("Радиус: ");
        scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    public Point scanAndGetPoint() {
        Point point = new Point();
        point.setX(scanX());
        point.setY(scanY());
        return point;
    }
}

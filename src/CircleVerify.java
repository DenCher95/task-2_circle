public class CircleVerify {

    public void verifyAllPointsInsideCircle(Circle circle, PointList pointList) {
        for (Point point : pointList.getListOfPoints()) {
            if (point.getDistanceToAnotherPoint(circle.getCenter()) < circle.getRadius()) {
                System.out.println("Точка лежит внутри окружности. Ее координаты х = " + point.getX() + ", у = " + point.getY());
            }
        }
    }
}
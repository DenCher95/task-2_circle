import java.util.ArrayList;
import java.util.List;

public class PointList {
    private List<Point> listOfPoints = new ArrayList<>();
    private boolean cycleBoolean;
    private int answer;
    ScannerFunctions scannerFunctions = new ScannerFunctions();

    public List<Point> getListOfPoints() {
        return listOfPoints;
    }

    private void addPoint(Point point) {
        listOfPoints.add(point);
    }

    public PointList() {
        addPoint(scannerFunctions.scanAndGetPoint());
        cycleBoolean = true;
        while (cycleBoolean) {
            answer = scannerFunctions.continueOrBrake();
            if (answer == 1) {
                addPoint(scannerFunctions.scanAndGetPoint());
            } else if (answer == 2) {
                cycleBoolean = false;
            } else {
                System.out.println("Введите 1 или 2");
            }
        }
    }
}

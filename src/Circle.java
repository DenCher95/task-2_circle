import java.util.Objects;

public class Circle {
    private Point center = new Point();
    private double radius;
    ScannerFunctions scannerFunctions = new ScannerFunctions();

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public Circle() {
        System.out.println("Введите координаты точки центра окружности");
        center.setX(scannerFunctions.scanX());
        center.setY(scannerFunctions.scanY());
        setRadius(scannerFunctions.scanCircleRadius());
    }
}
